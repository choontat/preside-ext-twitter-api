<cfparam name="args.title"         default="" />
<cfparam name="args.accounts"      default="" />
<cfparam name="args.count"         default=5 />
<cfparam name="args.show_replies"  default=false />
<cfparam name="args.show_retweets" default=false />

<cfoutput>#translateResource( uri='widgets.twitter_feed:title' )#</cfoutput>