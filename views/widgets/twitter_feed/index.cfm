<cfparam name="args.title"          default="" />
<cfparam name="args.renderedTweets" default="" />

<cfoutput>
	<div class="twitter-feed-widget">
		<cfif len( trim( args.title ) )>
			<h2>#args.title#</h2>
		</cfif>

		<div class="twitter-feed-widget-tweets">
			#args.renderedTweets#
		</div>
	</div>
</cfoutput>