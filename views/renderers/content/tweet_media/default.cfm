<cfscript>
	tweetData        = args.tweetData ?: {};
	media            = args.media     ?: [];
	mediaType        = args.mediaType ?: "";
	mediaTypeClass   = replaceNoCase( mediaType, "_", "-", "all" );
	mediaCount       = media.len();
	multiple         = mediaCount > 1;
	columnBreakAfter = mediaCount == 4 ? 2 : 1;
</cfscript>
<cfoutput>
	<cfif media.len()>
		<div class="tweet-media tweet-media-#mediaTypeClass# tweet-media-#multiple ? 'multi' : 'single'#">
			<cfif multiple>
				<div class="tweet-media-multi-columns">
					<div class="tweet-media-multi-column">
			</cfif>

			<cfloop array="#media#" index="index" item="item">
				#media[ index ]#
				<cfif multiple && index == columnBreakAfter>
					</div>
					<div class="tweet-media-multi-column">
				</cfif>
			</cfloop>

			<cfif multiple>
					</div>
				</div>
			</cfif>
		</div>
	</cfif>
</cfoutput>