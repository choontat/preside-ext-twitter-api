<cfscript>
	tweetData      = args.tweetData      ?: {};
	renderedHeader = args.renderedHeader ?: "";
	renderedFooter = args.renderedFooter ?: "";
	renderedBody   = args.renderedBody   ?: "";
	renderedMedia  = args.renderedMedia  ?: "";
	renderedQuoted = args.renderedQuoted ?: "";
	isQuoted       = isTrue( args.isQuoted ?: "" );
	className      = "tweet";
	if ( isQuoted ) {
		className &= " tweet-quoted";
	}

	event.include( "/js/twitter/" );
</cfscript>
<cfoutput>
	<article class="#className#">
		#renderedHeader#
		#renderedBody#
		#renderedMedia#
		#renderedQuoted#
		#renderedFooter#
	</article>
</cfoutput>