<cffunction name="renderTweet" access="public" returntype="string" output="false">
	<cfreturn getSingleton( "twitterRenderingService" ).renderTweet( argumentCollection=arguments ) />
</cffunction>

<cffunction name="renderTweets" access="public" returntype="string" output="false">
	<cfreturn getSingleton( "twitterRenderingService" ).renderTweets( argumentCollection=arguments ) />
</cffunction>

<cffunction name="renderTweetDate" access="public" returntype="string" output="false">
	<cfreturn getSingleton( "twitterRenderingService" ).renderTweetDate( argumentCollection=arguments ) />
</cffunction>
