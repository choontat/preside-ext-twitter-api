component {
	private function index( event, rc, prc, args={} ) {
		var tweetFilter = {
			account = listToArray( args.accounts ?: "" )
		};
		if ( !isTrue( args.show_replies ?: "" ) ) {
			tweetFilter.is_reply = false;
		}
		if ( !isTrue( args.show_retweets ?: "" ) ) {
			tweetFilter.is_retweet = false;
		}

		args.renderedTweets = renderTweets(
			  filter  = tweetFilter
			, maxRows = ( args.count ?: 5 )
		);

		return renderView( view="widgets/twitter_feed/index", args=args );
	}

	private function placeholder( event, rc, prc, args={} ) {
		return renderView( view="widgets/twitter_feed/placeholder", args=args );
	}
}
